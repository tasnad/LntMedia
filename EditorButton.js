var customizeToolbar = function ()
{
    // Adds a button to the toolbar, which replaces links in selected text with lntmedia tags.
    // The selection is assumed to contain one or more links, e.g.:
    // text before (will stay)[[:file:a.mp4]] text in between (will be DELETED) [[:file:a.ogv]] [[:file:a.webm]]text after (will stay)
    //
    // The result will be an lntmedia tag and the surrounding text in teh seelction:
    // text before (will stay)<lntmedia>
    //   file:a.mp3
    //   file:a.ogg
    // </lntmedia>text after (will stay)
    //
    // or
    // text before (will stay)<lntmedia>file:a.mp3</lntmedia>text after (will stay)
    // for only one file.
    $('#wpTextbox1').wikiEditor('addToToolbar', {
        section: 'main',
        group: 'insert',
        tools: {
            "convertToLntMedia": {
                label: 'Wrap selected media links in lntmedia tags and preserve text before and after',
                type: 'button',
                icon: 'extensions/LntMedia/ToolbarIcon.png',
                action: {
                    type: 'callback',
                    execute: function(context) {
                        // get selected text
                        var sel = $('#wpTextbox1').textSelection('getSelection');

                        // get the localized version of "file"
                        var cfg = mw.config;
                        var namespaces = cfg.get("wgFormattedNamespaces")
                        var fileId = cfg.get("wgNamespaceIds")["file"]
                        var lclFile = namespaces[fileId]

                        // find links in the selection and match only their filenames after the "file:" specifier (or "file"'s localized version)
                        // /\[{2}:?(?:file|datei):(.+?)(?:\|.*?)*\]{2}/gi
                        var findLinksRE = new RegExp("\\[{2}:?(?:file|" + lclFile + "):(.+?)(?:\\|.*?)*\\]{2}", "gi");

                        var mediaLinks = [];
                        var preText, tagText, postText;

                        var match = findLinksRE.exec(sel);
                        if (match != null)
                            var firstPos = match.index;
                        var lastPos = 0;
                        while(match != null)
                        {
                            mediaLinks.push(match[1]);
                            if (match.index + match[0].length > lastPos)
                                lastPos = match.index + match[0].length - 1;
                            match = findLinksRE.exec(sel);
                        }
                        preText = sel.substr(0, firstPos);
                        postText = sel.substr(lastPos+1);

                        tagText = "<lntmedia>"
                        if (mediaLinks.length > 1)
                        {
                            tagText += "\n"
                            for (let i = 0; i < mediaLinks.length; i++)
                                tagText += "  file:" + mediaLinks[i] + "\n";
                        }
                        else {
                            tagText += "file:" + mediaLinks[0];
                        }
                        tagText += "</lntmedia>";

                        // replace the selected text
                        $('#wpTextbox1').textSelection('encapsulateSelection', {pre: preText + tagText + postText, replace: true});
                    }
                }
            }
        }
    });
};



// Check if view is in edit mode and that the required modules are available. Then, customize the toolbar.
if ($.inArray(mw.config.get('wgAction'), ['edit', 'submit', 'formedit']) !== -1)
{
    mw.loader.using('user.options').then(function () {
        // This can be the string "0" if the user disabled the preference
        if (mw.user.options.get('usebetatoolbar') == 1)
        {
            $.when(
                mw.loader.using('ext.wikiEditor.toolbar'), $.ready
            ).then(customizeToolbar);
        }
    });
}
