Embeds media (given as Wiki reference) in HTML5 <audio> and <video> tags.



Installation
===========
Place files in extension/LntMedia and add in LocalSettings.php:
wfLoadExtension('LntMedia');
$wgUseSiteJs = true;
$wgLntMediaEnableButton = true;



Usage
===========
<lntmedia height="720" width="1280">
  file:a.mp4
  Datei:a.ogv
</lntmedia>

The height and width parameters are optional.



Wiki Editor Button
==================
To enable a button in the wiki editor which wraps selected media file lines in <lntmedia> tags, set
$wgLntMediaEnableButton = true;



Notes
=============
* currently, only the following file extensions are supported:
   mp4, webm, ogv videos and mp3, ogg, wav audio files.
   However, it's straight forward to add others in LntMedia.php.
* The first file in the newline separated list in the <lntmedia> environment decides whether
   it's an audio or video file and thus if an <audio> or <video> HTML5 tag is created.
