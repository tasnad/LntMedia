<?php
// @author Tasnad Kernetzky (tasnad dot kernetzky at tum dot de)
// @copyright Copyright © 2017, Tasnad Kernetzky
// @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 2.0 or later
// Forked from Seung Park's Html5mediator

// Embeds media (given as Wiki reference) in HTML5 <audio> and <video> tags.
// Usage:
// <lntmedia height="720" width="1280">
//   file:a.mp4
//   Datei:a.ogv
// </lntmedia>
//
// The height and width parameters are optional.
//
// Notes:
// 1) currently, only the following extensions are supported:
//    mp4, webm, ogv videos and
//    mp3, ogg, wav audio files. However, it's straight forward to add others in this script.
// 2) The first file in the newline separated list in the <lntmedia> environment decides whether
//    it's an audio or video file and thus if an <audio> or <video> HTML5 tag is created.

// exit if called directly
if (!defined('MEDIAWIKI'))
    die();

$wgExtensionCredits['LntMedia'][] = array(
    "path" => __FILE__,
    "name" => "LntMedia",
    "description" => "Embeds audio and video files by using html5 tags and www.html5media.info scripts.",
    "author" => "Tasnad Kernetzky"
    );

// Register the registration function
$wgHooks['ParserFirstCallInit'][] = 'LntMediaC::init';



class LntMediaC {
    static function init($parser)
    {
        // register the <lntmedia> tag for us and call our parser
        $parser->setHook('lntmedia' , 'LntMediaC::parse');
        return true;
    }

    static function addButtonToEditor(EditPage $editPage, OutputPage $output )
    {
        global $wgLntMediaEnableButton;
        if ($wgLntMediaEnableButton == true)
        {
            $output->addModules('ext.LntMedia');
        }
    }

    // This function is originally from the
    // MediawikiPlayer by Swiftlytilting.
    // It converts MediaWiki's "file:" tags into fully-valid URLs or just passes on the given url.
    //
    // If $mediaFile starts with "file:" or the localized version of that, it's full url is returned or an empty string if it does not exist.
    // Otherwise $mediaFile is returned.
    static function fileToUrl($mediaFile)
    {
        global $wgContLang;

        // load international name of File namespace
        $namespaceNames = $wgContLang->getNamespaces();
        $fileNS = strtolower($namespaceNames[NS_FILE]);
        // split by colons to get the file type
        $filePts = explode(":", $mediaFile);

        // check to see if a file was specified
        if ( count($filePts) > 1)
        {
            $ns = strtolower($filePts[0]);
            if ($ns == 'file' || $ns == $fileNS)
            {
                // join the file parts except the file type string
                $fileName = implode(":", array_slice($filePts, 1));
                $fileObj = wfFindFile($fileName);

                if ($fileObj)
                {
                    $fileUrl = $fileObj->getFullURL();
                    return $fileUrl;
                }
                else
                {
                    return "";
                }
            }
        }
        return $mediaFile;
    }



    // parse the tag's text and add an html5 <audio> or <video> tag
    static function parse($data, $params, $parser, $frame)
    {
        // escape from XSS vulnerabilities
        foreach ($params as $param => $paramval)
        {
            $params[$param] = htmlspecialchars($paramval);
        }
        $data = htmlspecialchars($data);

        $mediaUrls = array();
        foreach(explode("\n", $data) as $fileName)
        {
            // trim lines
            $fileName = trim($fileName);
            // continue for empty lines
            if (strlen($fileName) == 0)
                continue;

            $mediaUrl = LntMediaC::fileToUrl($fileName);
            // check if the file was found
            if (strlen($mediaUrl) == 0)
                return 'LntMedia: error loading file ' . Xml::encodeJsVar($mediaUrl) . "\n";

            // validate the URL
            if (!filter_var($mediaUrl, FILTER_VALIDATE_URL))
            {
                // allow urls without protocols
                if (!filter_var("http:" . $mediaUrl, FILTER_VALIDATE_URL))
                {
                    return 'LntMedia: not a valid URL: ' . $mediaUrl . "\n";
                }
            }

            // append to the file list
            $mediaUrls[] = $mediaUrl;
        }

        // find out if this is audio or video by the file extension of the first file
        $ext = pathinfo($mediaUrls[0], PATHINFO_EXTENSION);
        if (in_array($ext, array("mp3", "ogg", "wav")))
            $isAudio = True;
        elseif (in_array($ext, array("mp4", "webm", "ogv")))
            $isAudio = False;
        else
            return 'LntMedia: not a media file extension: ' . $ext . "\n";

        // Write out the actual HTML
        $retStr = '<style type="text/css">.video_w_border {border:3px solid grey;}</style>';
        // this can be used to support old browsers (adds flash if there is no html5 support)
        // $retStr .= "<script src=\"//api.html5media.info/1.2.2/html5media.min.js\"></script>";

        // <audio or <video opening tag
        if ($isAudio)
            $retStr .= "<audio controls";
        else
            $retStr .= "<video controls";

        // fill the tag with parameters given in the wiki
        $preloadGiven = False;
        $classGiven = False;
        foreach ($params as $param => $paramval)
        {
            // check if if some tags are given
            if (strcmp($param, "preload") == 0)
                $preloadGiven = True;
            if (strcmp($param, "class") == 0)
                $classGiven = True;

            $retStr .= " " . $param . '="' . $paramval . '"';
        }
        // append predefined tags if there is no override given
        if (!$preloadGiven)
            $retStr .= ' preload="auto"';
        if (!$classGiven)
        {
            if (!$isAudio)
                $retStr .= ' class="video_w_border"';
        }

        // close the opening tag
        $retStr .= ">";

        // add files
        foreach ($mediaUrls as $mediaUrl)
        {
            $retStr .= '<source src="' . $mediaUrl . '"';

            if (substr($mediaUrl, -4) === ".mp4")
                $retStr .= ' type="video/mp4"';
            elseif (substr($mediaUrl, -4) === ".ogv")
                $retStr .= ' type="video/ogg"';
            elseif (substr($mediaUrl, -5) === ".webm")
                $retStr .= ' type="video/webm"';
            elseif (substr($mediaUrl, -4) === ".mp3")
                $retStr .= ' type="audio/mpeg"';
            elseif (substr($mediaUrl, -4) === ".ogg")
                $retStr .= ' type="audio/ogg"';
            elseif (substr($mediaUrl, -4) === ".wav")
                $retStr .= ' type="audio/wav"';

            $retStr .= ' />';
        }

        // closing tag
        if ($isAudio)
            $retStr .= "</audio>";
        else
            $retStr .= "</video>";

        return $retStr;
    }
}